<?php

namespace Mojomaja\Component\Skurl;

class Client
{
    private $ch;

    public function __construct()
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    public function get($url, $qs = null)
    {
        if (is_array($qs))
            $qs = http_build_query($qs);

        return $this->exec($url . '?' . $qs, 'GET');
    }

    public function post($url, $data)
    {
        return $this->exec($url, 'POST', $data);
    }

    public function put($url, $data)
    {
        return $this->exec($url, 'PUT', $data);
    }

    private function exec($url, $verb, $data = null)
    {
        curl_setopt_array($this->ch, [
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => $verb,
            CURLOPT_POSTFIELDS      => $data
        ]);
        if (($result = curl_exec($this->ch)) !== false)
            return $result;

        throw new CUrlException(curl_error($this->ch), curl_errno($this->ch));
    }
}
